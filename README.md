# my-cellebrite-task

**Presentation**

The goal is to offer a working ci/cd pipeline for you to test.

The application is very simple and made with Python and Flask.
It serves a very basic website in the url http://ec2-18-196-158-57.eu-central-1.compute.amazonaws.com:5000/

_**To see changes of the flow, i recommend going to the app.py file and changing the text to anything**_.
_**In around 1-3 minutes, the changes should be seen in the application.**_


Flow chart can be found in this folder in aws-flowchart.png
The Lambda code can be found in the lambda.txt inside this folder.


**Flow chart steps**:
- Programmer will commit code to the Gitlab Repository
- The repository is connected through a Webhook and access token to Jenkins Master hosted on an EC2 instance and will trigger the build
- Slave will be activated and will start building the image based on the Dockerfile and the requirements.txt
- After the build is finished it will upload the image to ECR
- Cloudwatch is configured to monitor for any pushes to ECR
- Once Cloudwatch rule is triggered, it will start a Lambda function that will deploy the docker image to the Application Host EC2
- The Lambda function will also get the instance id dynamically and manage the docker containers and do a rollback update

**Security mentions**:
- The Jenkins Master is accessible through SSH for initial setup and HTTP port
- The Slave is accessible only from the SG group of the Master
- The EC2 Application host is accessible with SSM Agent and port 5000 for the application
- Each EC2 instance has a custom specific Role to access just the resources it needs
- Lambda function also has a custom Role to access Cloudwatch, SSM commands and EC2 read permissions for the deployment
- No Default VPC, SG, Subnet or IGW was used in the setup to have more control
- Different EC2 key pairs were used to access the instances

**Things to improve**:
- The Jenkins can be deployed to EKS and the slaves inside Nodes
- The Nodes can be transfered to private subnets, this way we won't need a public IP to do OS updates ( in the current setup it is protected from the outside )
- Output of the Lambda deployments can be saved in a S3 bucket for monitoring
- Implement versioning in a file inside the Application and have that as a tag when commits are done by Programmers so that we can deploy specific versions and have the option to rollback instead of just having the latest one
- I did not use different branches here as it was much faster to test directly, but this should be avoided and a proper merging procedure needs to be in place
- EC2 Key pairs need to have a rotation period if they are to be used ( around 60-90 days should be ok )
- HTTPS should be used for the application, but this is very application orientated and not the subject here

